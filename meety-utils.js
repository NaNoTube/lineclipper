/**
 * Craetes css selector from DOM element
 */
function getPath(node, path) {
    // The first time this function is called, path won't be defined.
    if (path == undefined) {
        path = '';
    }

    // If this element is <html> we've reached the end of the path.
    if (node.nodeName.toLowerCase() == 'html') {
        return 'html' + path;
    }

    // Add the element name.
    var cur = node.nodeName.toLowerCase();

    // Determine the IDs and path.
    var id = node.getAttribute('id');
    var cls = node.getAttribute('class');

    // Add the #id if there is one.
    if (id != null) {
        cur += '#' + id;
    }

    // Add any classes.
    if (cls != null) {
        cur += '.' + cls.split(/[\s\n]+/).join('.');
    }

    // Recurse up the DOM.
    return getPath(node.parentElement, ' > ' + cur + path);
};

/**
 * Get user range 
 */
function getUserRange() {
    var selection = document.getSelection();
    if (selection.isCollapsed) {
        return null;
    }
    var range = selection.getRangeAt(0);
    return range;
}

function getRangeData(r) {
    return {
        "startOffset": r.startOffset,
        "endOffset": r.endOffset,
        "startPathFromRange": startPathFromRange(r),
        "endPathFromRange": endPathFromRange(r)
    };
}
/**
 * Get a css path from text node
 */
function pathFromTextNode(textNode) {
    return getPath(textNode.parentElement)
}

function startPathFromRange(range) {
    return pathFromTextNode(range.startContainer);
}

function endPathFromRange(range) {
    return pathFromTextNode(range.endContainer);
}

function lineClipper(userRange) {

    var data = getRangeData(userRange);
    var jsonStr = JSON.stringify(data);

    var memo = $("#memo").val();
    var title = $("#title").val();
    var clip = $("#clip").val();
}

function createDialogForm() {

    var userRange = getUserRange();

    var d = $('<div id=dialog-form title="Input your memo"></div>');

    var p = $('<p></p>');

    var f = $('<form></form>');

    var memoLabel = $('<label></label>');
        memoLabel.text('Memo');

    var titleLabel = $('<label></label>');
        titleLabel.text('Title'); 

    var clipLabel = $('<label></label>');
        clipLabel.text('Clip');

    var titleInput = $('<input type=text name=title id=title class="text ui-widget-content ui-corner-all" />');
        titleInput.attr('value', document.title);

    var memoInput = $('<input type=text name=memo id=memo class="text ui-widget-content ui-corner-all" />');

    var clipInput = $('<input type=text name=clip id=clip class="text ui-widget-content ui-corner-all" readonly=true />');
        clipInput.attr('value', userRange.commonAncestorContainer.data.slice(userRange.startOffset,userRange.endOffset));


    $('#clip').val(userRange.commonAncestorContainer.data.slice(userRange.startOffset,userRange.endOffset));


    titleLabel.append(titleInput);
    memoLabel.append(memoInput);
    clipLabel.append(clipInput);
    f.append(titleLabel);
    f.append(memoLabel);
    f.append(clipLabel);
    d.append(p);
    d.append(f);

    $('body').append(d);

    return userRange;
}
function removeAll() {
    $('#dialog-form').remove();
}

function showDialogForm(userRange) {

    // Create as dialog
    $('#dialog-form').dialog({
        autoOpen: true,
        height: 400,
        width: 350,
        modal: true,
        buttons: {
            'Add memo': function (userRange) {
                return function(){
                    lineClipper(userRange);
                    $(this).dialog('close');
                };
            },
        },
            close: function() {
                removeAll();
            }
    });
}

/**
 * Inject jQuery into source
 */
/*
function injectjQuery() {
    var jquery = document.createElement('script');
        jquery.src = 'http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js';
    var jqueryUI = document.createElement('script');
        jqueryUI.src = 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js';
    var jqueryUICSS = document.createElement('link');
        jqueryUICSS.href = "http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css";
        jqueryUICSS.rel = "stylesheet";
    document.getElementsByTagName('head')[0].appendChild(jquery);
    document.getElementsByTagName('head')[0].appendChild(jqueryUI);
    document.getElementsByTagName('head')[0].appendChild(jqueryUICSS);
}
*/
/**
 * Inject CSS into source
 */
/*
function injectCSS() {
    var css = $('<style/>');
    css.html(
        "label,\n" +
        "input {\n" +
        "    display: block\n" +
        "}\n" +
        "input.text {\n" +
        "    margin-bottom: 12px;\n" +
        "    width: 95%;\n" +
        "    padding: .4em;\n" +
        "}\n" +
        "fieldset {\n" +
        "    padding: 0;\n" +
        "    border: 0;\n" +
        "    margin-top: 25px;\n" +
        "}\n" +
        "h1 {\n" +
        "    font-size: 1.2em;\n" +
        "    margin: .6em 0;\n" +
        "}\n" +
        "div#users-contain {\n" +
        "    width: 350px;\n" +
        "    margin: 20px 0;\n" +
        "}\n" +
        "    div#users-contain table {\n" +
        "        margin: 1em 0;\n" +
        "        border-collapse: collapse;\n" +
        "        width: 100%;\n" +
        "    }\n" +
        "        div#users-contain table td,\n" +
        "        div#users-contain table th {\n" +
        "            border: 1px solid #eee;\n" +
        "            padding: .6em 10px;\n" +
        "            text-align: left;\n" +
        "        }\n" +
        ".ui-dialog .ui-state-error {\n" +
        "    padding: .3em\n" +
        "}");
    $('head').append(css);
}
*/
function eventHandler(){
    // Prepare
//    injectjQuery();
//    injectCSS();
    
    // Create form's container
    r = createDialogForm();
    showDialogForm(r);
    
    // TODO: Delete injected items
}
chrome.browserAction.onClicked.addListener(function(tab) {
    chrome.tabs.executeScript({
        code: 'eventHandler();'
    });
});
//document.addEventListener('DOMContentLoaded', eventHandler());
