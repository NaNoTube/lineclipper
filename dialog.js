
/**
 * Craetes css selector from DOM element
 */
function getPath(node, path) {
    // The first time this function is called, path won't be defined.
    if (path == undefined) {
        path = '';
    }

    // If this element is <html> we've reached the end of the path.
    if (node.nodeName.toLowerCase() == 'html') {
        return 'html' + path;
    }

    // Add the element name.
    var cur = node.nodeName.toLowerCase();

    // Determine the IDs and path.
    var id = node.getAttribute('id');
    var cls = node.getAttribute('class');

    // Add the #id if there is one.
    if (id != null) {
        cur += '#' + id;
    }

    // Add any classes.
    if (cls != null) {
        cur += '.' + cls.split(/[\s\n]+/).join('.');
    }

    // Recurse up the DOM.
    return getPath(node.parentElement, ' > ' + cur + path);
};

/**
 * Get user range 
 */
function getUserRange() {
    var selection = document.getSelection();
    if (selection.isCollapsed) {
        return null;
    }
    var range = selection.getRangeAt(0);
    return range;
}

function getRangeData(r) {
    return {
        "startOffset": r.startOffset,
        "endOffset": r.endOffset,
        "startPathFromRange": startPathFromRange(r),
        "endPathFromRange": endPathFromRange(r)
    };
}
/**
 * Get a css path from text node
 */
function pathFromTextNode(textNode) {
    return getPath(textNode.parentElement)
}

function startPathFromRange(range) {
    return pathFromTextNode(range.startContainer);
}

function endPathFromRange(range) {
    return pathFromTextNode(range.endContainer);
}

function lineClipper(userRange) {

    var data = getRangeData(userRange);
    var jsonStr = JSON.stringify(data);

    var memo = $("#memo").val();
    var title = $("#title").val();
    var clip = $("#clip").val();
}

function createDialogForm() {

    var userRange = getUserRange();

    var d = $('<div id=dialog-form title="Input your memo"></div>');

    var p = $('<p></p>');

    var f = $('<form id=lineClipperForm></form>');

    var memoLabel = $('<label></label>');
        memoLabel.text('Memo');

    var titleLabel = $('<label></label>');
        titleLabel.text('Title'); 

    var clipLabel = $('<label></label>');
        clipLabel.text('Clip');

    var titleInput = $('<input type=text name=title id=title class="text ui-widget-content ui-corner-all" />');
        titleInput.attr('value', document.title);

    var memoInput = $('<input type=text name=memo id=memo class="text ui-widget-content ui-corner-all" />');

    var clipInput = $('<input type=text name=clip id=clip class="text ui-widget-content ui-corner-all" readonly=true />');
        clipInput.attr('value', userRange.commonAncestorContainer.data.slice(userRange.startOffset,userRange.endOffset));


    $('#clip').val(userRange.commonAncestorContainer.data.slice(userRange.startOffset,userRange.endOffset));


    titleLabel.append(titleInput);
    memoLabel.append(memoInput);
    clipLabel.append(clipInput);
    f.append(titleLabel);
    f.append(memoLabel);
    f.append(clipLabel);
    d.append(p);
    d.append(f);

    $('body').append(d);

    return userRange;
}

function removeForm() {
    $('#dialog-form').remove();
}

function highlightRange(userRange) {
}

function showDialogForm(userRange) {

    // Create as dialog
    $('#dialog-form').dialog({
        autoOpen: true,
        height: 400,
        width: 350,
        modal: true,
        buttons: {
            'Add memo': function (userRange) {
                return function(){
                    lineClipper(userRange);
                    $(this).dialog('close');
                };
            },
        },
            close: function() {
                removeForm();
            }
    });
}
function eventHandler(){
    // Prepare
    
    // Create form's container
    r = createDialogForm();
    highlightRange(r);
    showDialogForm(r);

    
    // TODO: Delete injected items
}
