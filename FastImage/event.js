var trigger_key = 113; // F1 key
var download_on = false;

function notifyOn(){
    alert('ON');
}

function notifyOff(){
    alert('OFF');
}

function doKeyPress(e){
    if (e.keyCode == trigger_key){
        if (download_on){
            download_on = false;
            notifyOff();
        } else{
            download_on = true;
            notifyOn();
        }
    }
}

function dowloadImage(img, url){
    var name = filename(url);
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'arraybuffer';
    xhr.onload = function(e){
        if (this.status == 200){
            var type = xhr.getResponseHeader('Content-Type');
            var blob = new Blob([new Uint8Array(this.response)],
                {type:type});
            saveAs(blob, name);
        }

    }
    xhr.send();
}

function onClickImage(e){
    if (download_on){
        e.preventDefault();
        dowloadImage(this, $(this).attr('src'));
        return false;
    }
}
function onClickA(e){
    if(download_on){
        e.preventDefault();
        return false;
    }
}

$(document).ready(function(){
    $(window).on('keyup', Number.MAX_VALUE, doKeyPress);
    $('img').click(onClickImage);
    $('a').click(onClickA);
});
